# Sole Geisha

Selbstbauprojekt einer Sole-Wasser-Wärmepumpe, angelehnt an die Luft-Wasser-Wärmepumpe "Geisha" von Panasonic

Entstanden in [diesem](https://www.haustechnikdialog.de/Forum/t/231091/R290-Sole-WP) Thread des HTD-Forums

## Die Wärmepumpe

### **Anforderungen an die Wärmepumpe**

#### Must haves:

* Kältemittel R290 (Propan) wegen "Selbstbaufreundlichkeit"
* max. Leistung ca. 5kW
* Produziert in EU
* klein und Wandaufhängung geeignet
* geringe Lautstärke!
* Passive cooling mit nur mit einem extra PWT, ohne extra Pumpe
* leistungsstarke Steuerung
* umfassendes Logging der Betriebsdaten

#### Shoule haves:

* Active Cooling
* möglichst geringer unterer Modulationsgrad
* Innenaufstellung
* intelligente Ölsumpfheizung
* Wetterprognosen in der Steuerung berücksichtigen, intelligente PV-Ausnutzung

#### Nice to haves:

* max 150Gr R290
* Preisrahmen bis 4000 €

Das Designziel soll sein, mit 150 g R290 auszukommen, um so die Innenaufstellung zu ermöglichen. Wird dieses Ziel nicht erreicht, so soll eine Außenaufstellung in Betracht gezogen werden.

### Komponenten

Zusammenfassung der Komponenten aus dem Forum

#### Literatur

[ISE Pressemitteilung](https://www.ise.fraunhofer.de/de/presse-und-medien/presseinformationen/2019/waermepumpe-mit-dem-klimafreundlichen-kaeltemittel-propan-fuer-die-aufstellung-im-haus-entwickelt.html)

[ISE Artikel](https://www.ise.fraunhofer.de/content/dam/ise/de/documents/presseinformationen/2019/2719_ISE_d_PI_Waermepumpe_mit_klimafreundlichem_Kaeltemittel.pdf)

#### Verdichter

[Copeland R290 Programm, Expansionventile etc](https://climate.emerson.com/documents/product-guide-for-propane-applications-en-gb-4213008.pdf)

[Copeland Scroll Kompressoren](https://climate.emerson.com/documents/copeland-scroll-for-r290-comfort-applications-fixed-variable-speed-models-en-gb-4839784.pdf)

[Mitsubishi SIAM Kompressoren](https://siamcompressor.com/index.php/product/r290-inverter-rotary)

[Mitsubishi Kompressoren Übersicht](https://innovations.mitsubishi-les.com/files/pdf/ME_Kompressorenbroschuere_A4_2016_RZ_web.pdf)

[Hitatchi Highly Twin-Rotary](https://bit.ly/3bs6CzM)  

#### Inverter

[Danfosss fc 302](http://files.danfoss.com/download/Drives/MG38A203.pdf)

[Automatic Motor Adaptation for PM motors](https://www.youtube.com/watch?v=QMlPVQQm4OI)

[96%](http://files.danfoss.com/download/Drives/MG33AS03.pdf)

[Carel](https://chillventa.carel.com/power-psd2-dc-inverter-with-embedded-compressor-protectio)

[LS Control](https://www.joap.dk/en/products/frekvens-omformere)

#### Expansionsventil

#### Verdampfer & Kondensator

Plattenwärmetauscher, möglichst asymmetrisch 

[AlphaLaval FlexFlow](https://www.alfalaval.de/produkte/waermeuebertragung/plattenwaermetauscher/geloetete-plattenwaermetauscher/cb/)

[SWEP AsyMatrix](https://www.swep.de/technologie/innovative-plattentechnologie/)



## Die Steuerung

Zur Zeit wird die Eigenentwickelung einer leistungsstarken Steuerung gegenüber fertig erhältlichen Steuerungen bevorzugt.

### Anforderungen an die Steuerung

#### Übersicht über zu erfassende Messwerte bzw Kommunikation

| **Messwert**                                       | **Art** | **Typ**                  | **Kommentar**                                                |
| -------------------------------------------------- | ------- | ------------------------ | ------------------------------------------------------------ |
| Temperatur Außen                                   | Eingang | ?                        | alternativ über KWL - Frischluft                             |
| Temperatur Innen                                   | Eingang | ?                        | Temperatur von einem Raumfühler, KNX-Raumkontroller, alternativ über KWL- Fortluft (vor dem Wärmetauscher) |
| Temperatur vor Kompressor                          | Eingang | PT100                    |                                                              |
| Temperatur nach Kompressor                         | Eingang | PT100                    |                                                              |
| Temperatur Verdampfer Eingang                      | Eingang | PT100                    |                                                              |
| Temperatur Verdampfer Ausgang                      | Eingang | PT100                    |                                                              |
| Hochdruckschalter                                  | Eingang | digital                  | Logisch UND-Verknüpfen mit ND-Schalter und auf Motorregler als Motor-Enable legen |
| Niederdruckschalter                                | Eingang | digital                  | Logisch UND-Verknüpfen mit HD-Schalter und auf Motorregler als Motor-Enable legen |
| Hochdruck                                          | Eingang | 4..20 mA                 |                                                              |
| Niederdruck                                        | Eingang | 4..20 mA                 |                                                              |
| Temperatur Kondensator Eingang                     | Eingang | PT100                    |                                                              |
| Temperatur Kondensator Ausgang                     | Eingang | PT100                    |                                                              |
| Temperatur Heizkreis Vorlauf                       | Eingang | PT100                    |                                                              |
| Temperatur Heizkreis Rücklauf                      | Eingang | PT100                    |                                                              |
| Temperatur Brauchwarmwasser                        | Eingang | PT100                    |                                                              |
| Temperatur Sole Vorlauf                            | Eingang | PT100                    |                                                              |
| Temperatur Sole Rücklauf                           | Eingang | PT100                    |                                                              |
| Temperatur Ölsumpf                                 | Eingang | PT100                    |                                                              |
| Stromzähler                                        | Eingang | digitaler Impuls         | Impulseingang für Arbeitszahlberechnung / Solaroptimierung / optional per Modbus |
| Wärmemengenzähler / Volumenstrom                   | Eingang | digitaler Impuls         | Impulseingang für Arbeitszahlberechnung                      |
| Temperatur KWL Abluft                              | Eingang | PT100                    |                                                              |
| Temperatur KWL Abluft                              | Eingang | PT100                    |                                                              |
|                                                    |         |                          |                                                              |
| Dreiwegeventil Umschaltung Brauchwasserererwärmung | Ausgang | Relais                   |                                                              |
| Dreiwegeventil Umschaltung 2. HK                   | Ausgang | Relais                   |                                                              |
| Umwälzpumpe Heizkreis                              | Ausgang | Digital OUT / Analog OUT | PWM oder 0-10 V                                              |
| Umwälzpumpe Sole                                   | Ausgang | Digital OUT / Analog OUT | PWM oder 0-10 V                                              |
| Ansteuerung des Motorregler für den Kompressor     | Ausgang | ?                        | Vorzugsweise seriell da dann akt. Parameter mit ausgelesen werden können, PWM oder 0..10 V ebenfalls denkbar |
| Dreiwegeventil Passiv Cooling                      | Ausgang | Relais                   |                                                              |
| Ansteuerung EEV                                    | Ausgang | Digital OUT              | PWM                                                          |
| Ansteuerung Ölsumpfheizung                         | Ausgang | Relais                   |                                                              |
|                                                    |         |                          |                                                              |
| RS232 / RS485                                      | IO      |                          | [Optional] Allgemeine serielle Schnittstelle, für übergeordnete Steuerung, Wärmemengenzähler, Energiemerter etc |
| RS232 / RS485                                      | IO      |                          | [Optional] Allgemeine serielle Schnittstelle, für übergeordnete Steuerung, Wärmemengenzähler, Energiemerter etc |
| USB                                                | IO      |                          | Zum Parametrieren des Reglers                                |
| Ethernet                                           | IO      |                          | [Optional] Zum Parametrieren / Monitoring des Reglers, Zugang für übergeordneten Regler |

#### Anforderungen an die Eingänge

- Robust
- Ordentliche Filterung, 50 Hz Netzunterdrückung
- Oberwellen der Motorreglung beachten
- Temperatursensoren werden vom Board versorgt, daher dort keine galv. Trennung nötig
- 4..20 mA werden ebenfalls vom Board versorgt, daher dort keine galv. Trennung nötig
- 0..10 V Eingänge mit Surpressordioden schützen, ordentliche Filterung und Überspannungsschutz Massepotential muss **zwingend** das Selbe sein, da galvanisch getrennte Analogwandler aufwendig und teuer sind
- Schlecht ausgeführte und abgesicherte Analogeingänge sind der einfachste Weg, den Regler durch Überspannung (Spannungstransienten, Bursts und Surges, leitungsgeführte Störungen etc.) zu zerstören!



### Anforderungen an die Ausgänge

TODOS:

- [ ] Spannungsbereich der PWM-Ausgänge klären
- [ ] galvanische Trennung? Hängt von Technologie ab, mit Relais einfach, mit Halbleiter schwieriger.
- [ ] Halbleiterausgang wird gebraucht für PWM, da Relais für PWM unbrauchbar

#### Anforderungen an die Temperatursensoren

- Messbereich: -20 bis +120°C
- Auflösung : 0,1° C
- absolute Genauigkeit: 0,1° C
- Messfrequenz: 10 Hz

Die Sensoren sollen im Feld einfach zu tauschen sein, ein Kalibrierung/Identifizierung der einzelnen Sensoren soll vermieden werden. Des Weiteren sollen die Sensoren von mehreren Herstellern lieferbar sein. Verschiedene Bauformen der Sensoren werden benötigt, auch in robuster Form wie zum Beispiel in IP67 oder als Rohranlegesensor. Darüber hinaus sollten die Sensoren betriebsbewährt sein.

Da Halbleitersensoren wie der DS18B20 leider grenzwertig bezüglich Temperaturbereich sind, wird als Technologie für die Temperaturmessung Sensoren auf [PT100-Basis](https://de.wikipedia.org/wiki/Platin-Messwiderstand) vorgeschlagen.

### Hardwarekonzept

Die Regelung soll so konzipiert werden, das eine einfache Basis-Steuerung der Wärmepumpe möglich ist. Darüber hinaus soll ein übergeordneter Regler erweiterte und innovative Regelungskonzepte ermöglichen. Dabei soll sicher gestellt werden, das die Wärmepumpe immer innerhalb ihrer Spezifikation betrieben wird,  das soll bedeuten, das vom Regler eine Plausibiltätskontrolle statt findet und evtl die Maschine überfordernde Anforderung zurück gewiesen wird.

Das Board soll von Hand bestückbar sein.

Da sich die Verwendung von SMD-Komponenten nicht vermeiden lassen wird, dabei soll darauf geachtet werden, das die Gehäusegröße der Komponenten so gewählt wird, das sie noch von Hand bestückbar sind.

Mini-Linux-SBC haben sind bezüglich der Rechenleistung unschlagbar, ein Trägerboard für den Raspberry Pi wäre denkbar. Leider haben sich Raspis und insbesondere die SD-Karten nicht als robust genug herausgestellt. Die Verwendung eines Linux-OS ist komfortabel, birgt aber eine gewisse Komplexität ins sich.

Also Alternative bieten sich Arduino-Boards an. Der Formfaktor für Erweiterung kann als Standard angesehen werden. Die Orginal-Arduinos benutzen ein 8-Bit µC von Atmel mit einer einstiegerfreundlichen IDE zur Softwareentwicklung. Leider ist diese Entwicklungsumgebung wenig geeignet für ernsthaft Steuer- und Regelungsaufgaben, da nebenläufige Prozesse nicht abgebildet werden können.

Leistungsfähigere 32-Bit-Controller werden unter anderem von ST in Form der Serie STM32 angeboten. 

Die Leistungspalette reich von sehr kleinen Controllern für einen Preis von unter einem Euro bis sehr leistungsfähigen Controllern mit Floating-Point-Hardware und 1MB integriertem RAM.

Besonders interessant sind die von ST angebotenen Eval-Boards [ST Nucleo](https://www.st.com/en/evaluation-tools/stm32-nucleo-boards.html). Sie bieten unter anderem ein Arduino-kompatible Erweiterungsstecker sowie einen sehr leistungsfähigen auf dem Board integrierten Debugger/Programmieradapter. Größere Boards und Controller stellen sogar einen RJ45 Ethernetanschluß bereit.

Ein weitere Vorteil der Nucleos liegt in der hohen Integration der Boards. Alle kritischen Hochfrequenzsignale sind bereits korrekt im Layout geführt. Ebenfalls ist die nicht immer unkritische Spannungsversorgung der STM32-Controller bereits fertig auf den Boards vorhanden.

Preislich sind die Nucle-Boards sehr attraktiv, stellenweise sind die fertigen Boards günstiger als der darauf verbaute Controller in Einzelstückzahlen. Die Verfügbarkeit der Boards ist ebenfalls über die üblichen großen Distributoren sehr gut.

Für die STM32 Line spricht ebenfalls die weite Verbreitung und hervorragende Unterstützung der Compiler-Toolchain. Die üblichen Echtzeitsbetriebssysteme wie FreeRTOS, ChibiOS, ArmEmbedded oder  Zephyr werden sehr gut unterstützt.

Daher wird vorgeschlagen, ein Trägerboard zu entwickeln, das die Auswertelektronik für die Sensorik enthält und als Träger für ein Prozessorboard dient.

#### Spannungsversorgung

- [ ] TODO: direkt an 230V oder an 24V? Hängt von ab was Regelung EEV braucht?

#### Messwererfassung

##### Temperaturerfassung

Aufgrund der Anforderungen an die Temperatursensoren werden PT100 Widerstandsthermometer in Dreileitertechnik vorgeschlagen.

Um die PT100 einfach auslesen zu können, wird die Verwendung externer AD-Wandler-Chips vorgeschlagen. Der Grund hierfür ist, das man dadurch unabhänig in der Wahl des konkreten Prozessors bleibt, da die Daten nur seriell per SPI übertragen werden. 

Ein vorgeschlagener Baustein dafür ist der [ADS124S08](https://www.ti.com/product/ADS124S08) mit 24 Bit-Wandler für ca 9,50 € alternativ aus der gleichen Familie der [ADS114S08](https://www.ti.com/product/ADS114S08?keyMatch=ADS114S08&tisearch=Search-EN-everything&usecase=part-number) mit 16 Bit-Wandler für 7,80 €. Beiden Bausteinen ist gemein, das sie bereits das komplette analoge Front-End beinhalten, was zum ratiometrischen Auslesen eines PT100 / PT1000 erforderlich ist. Praktisch sind die integrierten 50 / 60 Hz digitale Filter für die Netzunterdrückung

TI gibt hier ein [Referenz-Design](https://www.ti.com/lit/an/sbaa275/sbaa275.pdf?&ts=1589735537845) vor, mit verschiedenen Möglichkeiten der Zwei-, Drei- oder Vierleiteranbindung des Messwiderstands vor. [Hiermit](http://www.ti.com/lit/zip/sbac158) können die Werte für den Refernzwiderstand berechnet werden.

Mit einem ADS1x4S08 können vier PT100 eingelesen werden, das ergibt pro Kanal Kosten von ca 1,95 € für die 16-Bit-Version oder 2,40 € für die 24-Bit-Version.

Ebenfalls von TI gibt es [hier](http://www.ti.com/lit/an/slyt442/slyt442.pdf?&ts=1589839932431) eine Application Note für das ratiometrische Messen und das Gleichzeitige linearisieren von PT100/1000. Diese Lösung scheitert aber an den begrenzten Analogeingängen.

##### 4..20 mA

Der zu Messende Strom wird über einen einfachen Widerstand geführt und die daran abfallende Spannung gemessen. Sie kann über einen  [ADS114S08](https://www.ti.com/product/ADS114S08?keyMatch=ADS114S08&tisearch=Search-EN-everything&usecase=part-number) oder den AD des Microcontrollers gemessen werden. Es wird empfohlen, den [ADS114S08](https://www.ti.com/product/ADS114S08?keyMatch=ADS114S08&tisearch=Search-EN-everything&usecase=part-number) zu Verwenden, um vom konkreten Microcontrollerboard unabhängig zu sein. Mit dem [ADS114S08](https://www.ti.com/product/ADS114S08?keyMatch=ADS114S08&tisearch=Search-EN-everything&usecase=part-number) können bis zu zwölf Kanäle gemessen werden, so dass sich eine Aufteilung auf die 0..10 V Eingänge anbietet.

##### 0..10 V

Es gelten die gleichen Argumente für einen [ADS114S08](https://www.ti.com/product/ADS114S08?keyMatch=ADS114S08&tisearch=Search-EN-everything&usecase=part-number) wie bei der 4..20ma Wandlung. Zu beachten ist hier, das die Filterung, der Überspannungsschutz extrem gut ausgeführt werden müssen, da **keine Galvanische Trennung** der Potentiale vorliegt!

#### Kommunikation

##### RS485 

- [ ] TODO: klären ob galvanisch getrennt nötig, sonst einfacher Pegelwandler

##### RS232 

- [ ] TODO: klären ob galvanisch getrennt nötig, sonst einfacher Pegelwandler

##### USB

Der USB-Port des Nucleo kann zum Parametrieren / Monitoring benutzt werden

##### Ethernet

Je nach gewähltem Nucleo steht Ethernet zur Verfügung. Wird der Controller leistungsfähig genug gewählt, kann die Anbindung an einen übergeordneten Regler einfach erfolgen, auch ein Monitoring der Daten über beispielsweise MQTT ist realisierbar

### Regelungskonzept

- [ ] TODO: zusammenfassen aus Forum



### Lizenz

Es wird für dieses Projekt die "Creative Commons: BY-NC-SA: Namensnennung, nicht kommerziell, Weitergabe unter gleichen Bedingungen" gewählt. Nähere Infos und vollständige Lizens [hier](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de)

